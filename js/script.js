const tabla = document.querySelector("#listadoPaises tbody");

function listarPaises() {
  const url = "https://restcountries.eu/rest/v2/lang/es";
  fetch(url)
    .then((respuesta) => respuesta.json())
    .then((paises) => {
      paises.forEach((pais) => {
        //console.log(pais);
        const row = document.createElement("tr");
        row.innerHTML += `
            <td><a href="#" class="cta" onclick="btnmodal('${pais.region}')">${pais.name}</a></td>
            <td>${pais.capital}</td>
            <td>${pais.callingCodes}</td>
            <td>${pais.region}</td>
            <td>${pais.subregion}</td>
            <td><img src="${pais.flag}" width="30" height="40"></td>
            <td>${pais.cioc}</td>
        `;
        tabla.appendChild(row);
      });
    });
}

listarPaises();

function btnmodal(continente) {
  const cont = continente;
  Swal.fire({
    text: "Continente es: " + cont,
    confirmButtonText: "Cool",
  });
}
